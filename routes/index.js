const express = require("express");

const router = express.Router();
const { ensureAuthenticated, forwardAuthenticated } = require("../configs/auth");

router.get("/", ensureAuthenticated, (req, res) => {
  res.render("home");
});

router.get("/add", ensureAuthenticated, (req, res) => {
  res.render("add", {
    user: req.user,
  });
});
router.get('/added',ensureAuthenticated,(req,res)=>{
  res.render('added')
})
router.post('/add',ensureAuthenticated ,(req,res)=>{
    const {name , kind,value,synonyms}=req.body;
    //console.log(value);
   // console.log(synonyms);

    const dialogflow = require("dialogflow");

    const cred1 = require("../credentials.json");
    const cred2 = require("../credentials2.json");
    var cred, projectId;
    //console.log(req.user);
    
    if(req.user.name === "Sanchit" ){
        cred=cred1;
        projectId = "street-smarts-smnxba";
    }
    else{
        cred=cred2;
         projectId = "chaty-1-olru";
    }
    const entitiesClient = new dialogflow.EntityTypesClient({
        
        
      credentials: cred,
    });
   // console.log(cred);      
    
    const agentPath = entitiesClient.projectAgentPath(projectId);

    var entt=[];
    for (let i = 0; i < value.length; i++) {
        var syn = synonyms[i].split(",");
        console.log(syn);
        var enttt={
            value: value[i],
            synonyms: syn
        }
        //console.log(enttt)
        entt=[...entt , enttt]
    }
    //console.log(entt)
    // var syn=synonyms.split(",");
    // console.log(syn);

    

    let Enty = name + "EntityType";
    let reqs=name+"Request"
     Enty = {
      displayName: name,
      kind: kind,
      entities:entt,
    };
   reqs = {
      parent: agentPath,
      entityType: Enty,
    };
    
    entitiesClient
      .createEntityType(reqs)
      .then((response) => {
        console.log("Created new entity: ", JSON.stringify(response[0]));
        res.render("added");
      })
      .catch((err) => {
        console.error("Error creating entity : ", err);
        window.alert("Some Error Ocurred")
      });

      

})
module.exports = router;
