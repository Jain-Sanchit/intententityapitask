/* not working currently*/


"use strict";
const dialogflow = require("@google-cloud/dialogflow");
const cred = require("./credentials.json");

// Instantiates the Intent Client
const intentsClient = new dialogflow.IntentsClient({
  credentials: cred,
});
const agentPath = intentsClient.agentPath("street-smarts-smnxba");
const projectId="street-smarts-smnxba"
const intentId = "to delete";
async function deleteIntent(projectId,intentId) {
     const intentPath = intentsClient.intentPath(projectId, intentId);

  const request = {name: intentPath};

  // Send the request for deleting the intent.
  const result = await intentsClient.deleteIntent(request);
  console.log(`Intent ${intentPath} deleted`);
  return result;
  // [END dialogflow_delete_intent]
}

deleteIntent(projectId,intentId)