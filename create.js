const dialogflow = require("dialogflow");

const cred = require("./credentials.json");

const entitiesClient = new dialogflow.EntityTypesClient({
  credentials: cred,
});

const projectId = "street-smarts-smnxba";
const agentPath = entitiesClient.projectAgentPath(projectId);

// const cityEntityType={
//     displayName: 'city',
//     kind: 'KIND_MAP',
//     entities: [
//         {value: 'Ney York', synonyms:['New York','NYC']},
//         {value: 'Los Angeles', synonyms:['LA','Los Angeles', 'L.A.']}
//     ],
// }

// const cityRequest={
//     parent: agentPath,
//     entityType: cityEntityType
// };

// entitiesClient
// .createEntityType(cityRequest)
// .then(response =>{
//     console.log('Created new entity: ',JSON.stringify(response[0]));

// })

// const streetEntityType = {
//     displayName: 'street',
//     kind: 'KIND_MAP',
//     entities: [
//         { value: 'Broadway', synonyms: ['Broadway'] }
//     ],
// }

// const streetRequest = {
//     parent: agentPath,
//     entityType: streetEntityType
// };

// entitiesClient
//     .createEntityType(streetRequest)
//     .then(response => {
//         console.log('Created new entity: ', JSON.stringify(response[0]));

//     })
// .catch(err =>{
//     console.error('Error creating entity : ',err)
// })

const countryEntityType = {
  displayName: "country",
  kind: "KIND_MAP",
  entities: [
    {
      value: "United States",
      synonyms: ["US", "USA", "United States", "United States of America"],
    },
    { value: "India", synonyms: ["IN", "India", "Bharat"] },
  ],
};

const countryRequest = {
  parent: agentPath,
  entityType: countryEntityType,
};

entitiesClient
  .createEntityType(countryRequest)
  .then((response) => {
    console.log("Created new entity: ", JSON.stringify(response[0]));
  })
  .catch((err) => {
    console.error("Error creating entity : ", err);
  });
