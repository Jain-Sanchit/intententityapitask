const express=require('express')
const app=express()
const bodyParser=require('body-parser')
const mongoose=require('mongoose')
const passport=require('passport')

const hbs = require("hbs");
const flash = require("connect-flash");
const session = require("express-session");
app.use(bodyParser.urlencoded({ extended: true, useUnifiedTopology: true }));
app.use(bodyParser.json());

require("./configs/passport")(passport);

hbs.registerPartials(__dirname + "/views/partials");

hbs.registerHelper("ifnoteq", function (a, b, options) {
  if (a != b) {
    return options.fn(this);
  }
  return options.inverse(this);
});


const db = require("./configs/keys").mongoURI;

mongoose
  .connect(db, { useNewUrlParser: true, useUnifiedTopology: true })
  .then(() => console.log("MongoDB Connected"))
  .catch((err) => console.log(err));
// mongoose.connect(
//   config.dbURL,
//   { useNewUrlParser: true, useNewUrlParser: true, useUnifiedTopology: true },
//   () => {
//     console.log("COnnected to mongo");
//   }
// );
app.set("view engine", "hbs");
app.use(express.urlencoded({ extended: true }));

app.use(
  session({
    secret: "secret",
    resave: true,
    saveUninitialized: true,
  })
);
app.use(passport.initialize());
app.use(passport.session());

app.use(flash());

app.use(function (req, res, next) {
  res.locals.success_msg = req.flash("success_msg");
  res.locals.error_msg = req.flash("error_msg");
  res.locals.error = req.flash("error");
  next();
});


app.use('/',require('./routes/index'))
app.use('/users',require('./routes/users'))


app.listen(8080, () => {
  console.log("Running on 8080 !!");
});

