"use strict";
const dialogflow = require("@google-cloud/dialogflow");
const cred = require("./credentials.json");

// Instantiates the Intent Client
const intentsClient = new dialogflow.IntentsClient({
  credentials: cred,
});
    const agentPath = intentsClient.agentPath("street-smarts-smnxba");

    function main(projectId = "street-smarts-smnxba") {
      async function listIntents() {
        console.log(agentPath);

        const request = {
          parent: agentPath,
        };

        // Send the request for listing intents.
        const [response] = await intentsClient.listIntents(request);
        response.forEach((intent) => {
          console.log("====================");
          console.log(`Intent name: ${intent.name}`);
          console.log(`Intent display name: ${intent.displayName}`);
          console.log(`Action: ${intent.action}`);
          console.log(`Root folowup intent: ${intent.rootFollowupIntentName}`);
          console.log(
            `Parent followup intent: ${intent.parentFollowupIntentName}`
          );

          console.log("Input contexts:");
          intent.inputContextNames.forEach((inputContextName) => {
            console.log(`\tName: ${inputContextName}`);
          });

          console.log("Output contexts:");
          intent.outputContexts.forEach((outputContext) => {
            console.log(`\tName: ${outputContext.name}`);
          });
        });
      }

      listIntents();

    }

main(...process.argv.slice(2));