"use strict";


function main(
  projectId = "street-smarts-smnxba",
  displayName = "weather-intent",
  trainingPhrasesParts = [
    "Hello, What is weather today?",
    "How is the weather today?",
  ],
  messageTexts = ["Rainy", "Sunny"]
) {
  
  
   const dialogflow = require("@google-cloud/dialogflow");
const cred = require("./credentials.json");

   const intentsClient = new dialogflow.IntentsClient({
    credentials: cred,
  });

  async function createIntent() {
    
     const agentPath = intentsClient.agentPath("street-smarts-smnxba");

    const trainingPhrases = [];

    trainingPhrasesParts.forEach((trainingPhrasesPart) => {
      const part = {
        text: trainingPhrasesPart,
      };

      // Here we create a new training phrase for each provided part.
      const trainingPhrase = {
        type: "EXAMPLE",
        parts: [part],
      };

      trainingPhrases.push(trainingPhrase);
    });

    const messageText = {
      text: messageTexts,
    };

    const message = {
      text: messageText,
    };

    const intent = {
      displayName: displayName,
      trainingPhrases: trainingPhrases,
      messages: [message],
    };

    const createIntentRequest = {
      parent: agentPath,
      intent: intent,
    };

    // Create the intent
    const [response] = await intentsClient.createIntent(createIntentRequest);
    console.log(`Intent ${response.name} created`);
  }

  createIntent();

}
main(...process.argv.slice(2));
