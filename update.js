const dialogflow = require('dialogflow')

const cred = require('./credentials.json');

const entitiesClient = new dialogflow.EntityTypesClient({
    credentials: cred,
})

const projectId = 'street-smarts-smnxba'
const agentPath = entitiesClient.projectAgentPath(projectId)

class EntityNotFoundError extends Error {};


//checking if entity is thr
entitiesClient
.listEntityTypes({parent: agentPath})
.then(responses => {
    const resources=responses[0]
    for (let i = 0; i < resources.length; i++) {
        const entity=resources[i];
        if(entity.displayName === 'city'){
            return entity;
        }
        
    }
    throw new EntityNotFoundError();
})
.then(city =>{
    console.log('Found entity city : ',JSON.stringify(city));
    const updateEntityList=[
        { value: 'New York', synonyms: ['NYC','New York'] },
        {value:'Los Angeles',synonyms:['Los Angeles','LA','L.A.']},
        {value:'Chicago',synonyms:['Chicago']},
        { value: 'Houston', synonyms: ['Houston'] },
    ]
    city.entities=updateEntityList;
    const request={
        entityType: city,
        updateMask:{
            paths: ['entities']
        }
    }
    return entitiesClient.updateEntityType(request)    
})
.then(res => {
    console.log('Updates Entity type :', JSON.stringify(res[0]));
    
})
.catch(err =>{
    if(err instanceof EntityNotFoundError){
        console.error('Could not find entity named such')
        return;
    }
    console.error('Error Updating ',err)
})

