const LocalStrategy=require('passport-local').Strategy
const mongoose=require('mongoose')

const UserDb=require('../models/user')

module.exports = function (passport) {
  passport.use(
    new LocalStrategy({ usernameField: "email" }, (email, password, done) => {
      // Match user
      UserDb.findOne({
        email: email,
      }).then((userDb) => {
        if (!userDb) {
          return done(null, false, { message: "That email is not registered" });
        }
        if (password === userDb.password) {
          return done(null, userDb);
        } else {
          return done(null, false, { message: "password wrong" });
        }
      });
    })
  );
  passport.serializeUser(function (userDb, done) {
    done(null, userDb.id);
  });

  passport.deserializeUser(function (id, done) {
    UserDb.findById(id, function (err, userDb) {
      done(err, userDb);
    });
  });
};